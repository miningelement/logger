<?php
/**
 * Examples:
 *
 * $logger = new \Logger\Logger($_SERVER['DOCUMENT_ROOT'] . '/default.log');
 *
 * $logger->info("Info message");
 * $logger->alert("Alert message");
 * $logger->error("Error message");
 * $logger->debug("Debug message");
 * $logger->notice("Notice message");
 * $logger->warning("Warning message");
 * $logger->critical("Critical message");
 * $logger->emergency("Emergency message");
 */

namespace Miningelement;

use SplObjectStorage;
use \Psr\Log\AbstractLogger;
use \Psr\Log\LoggerInterface;
use \Miningelement\Routes\Route;

/**
 * Class Logger
 */
class Logger extends AbstractLogger implements LoggerInterface
{
    const ROOT_PATH = '/logs/';

    /**
     * @var SplObjectStorage Routes list
     */
    public $routes;

    /**
     * Конструктор
     *
     * @param string $filePath
     */
    public function __construct($filePath)
    {
        $this->routes = new SplObjectStorage();

        $this->routes->attach(new Routes\FileRoute(['filePath' => $_SERVER['DOCUMENT_ROOT'] . self::ROOT_PATH . $filePath]));
    }

    /**
     * @inheritdoc
     */
    public function log($level, string|\Stringable $message, array $context = []): void
    {
        foreach ($this->routes as $route) {
            if (!$route instanceof Route) {
                continue;
            }

            if (!$route->isEnable) {
                continue;
            }

            $route->log($level, $message, $context);
        }
    }
}
