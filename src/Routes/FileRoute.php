<?php

namespace Miningelement\Routes;

use Miningelement\Routes\Route;

/**
 * Class FileRoute
 */
class FileRoute extends Route
{
    /**
     * @var string Путь к файлу
     */
    public $filePath;
    /**
     * @var string Шаблон сообщения
     */
    public $template = "{date} {level} {message} {context}";

    /**
     * @inheritdoc
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        if (!file_exists($this->filePath)) {
            // создадим каталог
            CheckDirPath($this->filePath, true);

//            $saveCatalog = explode('/', $this->filePath);
//            unset($saveCatalog[count($saveCatalog) - 1]); // удалим последний элемент, являющийся файлом
//            $saveCatalog = implode('/', $saveCatalog);

//            $this->createDir($saveCatalog);
            unset($saveCatalog);
        }
    }

    /**
     * Создаст указанный путь
     *
     * @param $savePath
     */
    private function createDir($savePath)
    {
        if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $savePath)) {
            $arPath = explode('/', $savePath);

            $dir = '';
            foreach ($arPath as $value) {
                if (!empty($value)) {
                    $dir .= '/' . $value;
                    if (file_exists($dir)) {
                        continue;
                    }
                    mkdir($dir, 0755, true);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function log($level, $message, array $context = []): void
    {
        file_put_contents($this->filePath, trim(strtr($this->template, [
                '{date}' => $this->getDate(),
                '{level}' => $level,
                '{message}' => $message,
                '{context}' => $this->contextStringify($context, $level),
            ])) . PHP_EOL . PHP_EOL, FILE_APPEND);
    }
}
