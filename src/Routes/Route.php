<?php

namespace Miningelement\Routes;

use DateTime;
use \Psr\Log\AbstractLogger;
use \Psr\Log\LoggerInterface;

/**
 * Class Route
 */
abstract class Route extends AbstractLogger implements LoggerInterface
{
    /**
     * @var bool
     */
    public $isEnable = true;

    /**
     * @var string log date format
     */
    public $dateFormat = 'Y-m-d H:i:s';

    /**
     * @param array $attributes Атрибуты роута
     */
    public function __construct(array $attributes = [])
    {
        foreach ($attributes as $attribute => $value) {
            if (property_exists($this, $attribute)) {
                $this->{$attribute} = $value;
            }
        }
    }

    /**
     * Current date
     *
     * @return string
     * @throws \Exception
     */
    public function getDate()
    {
        return (new DateTime())->format($this->dateFormat);
    }

    /**
     * @param array  $context
     * @param string $level
     *
     * @return string
     */
    public function contextStringify(array $context = [], $level = '')
    {
        if ((count($context) === 1) && !empty($context[0]) && $level === 'debug') {
            $context = $context[0];
        }

        $contextOut = ($level === 'debug') ? PHP_EOL . print_r($context, true) : json_encode($context);

        return !empty($context) ? $contextOut : null;
    }
}
